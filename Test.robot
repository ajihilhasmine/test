*** Settings ***
Documentation    This is a test suite for testing negative scenarios of Optimy Login Page
Library	SeleniumLibrary
Test Setup    Go To Login Page
Test Teardown    Close Browser

*** Test Cases ***
TC1_LoginWithNullEmailAndPassword
    Input Text    ${EmailTextField}    ${EMPTY}
    Input Password    ${PasswordTextField}    ${EMPTY}
    Click Button    ${LoginButton}
    Element Should Be Visible    ${EmailRequiredMsg}
    Element Should Be Visible    ${PasswordRequiredMsg}

TC2_LoginWithIncorrectEmailAndPassword
    Input Text    ${EmailTextField}    ${IncorrectEmail}
    Input Password    ${PasswordTextField}    ${IncorrectPassword}
    Click Button    ${LoginButton}
    Wait Until Element Is Visible    ${LoginInvalid}

TC3_InputInvalidEmail
    Input Text    ${EmailTextField}    ${InvalidEmailInput}
    Press Keys    ${EmailTextField}    ENTER
    Element Should Be Visible    ${InvalidEmailMsg}

*** Variables ***
${Browser}    Chrome
${LoginURL}    https://login.optimyapp.com/
${EmailTextField} 	//*[@id="tab-login"]//input[contains(@name, 'email')]
${PasswordTextField} 	//*[@id="tab-login"]//input[contains(@name, 'password')]
${LoginButton}    //*[@id="tab-login"]/form/button
${EmailRequiredMsg}    //*[@id="tab-login"]//span[contains(@for,'email')]
${PasswordRequiredMsg}    //*[@id="tab-login"]//span[contains(@for,'password')]
${LoginInvalid}    //*[@id="login-invalid"]
${InvalidEmailMsg}    //span[contains(text(), 'Please enter a valid email address (e.g.: john.smith@gmail.com).')]
${InvalidEmailInput}    h@com
${IncorrectEmail}    test@gmail.com
${IncorrectPassword}    abc

*** Keywords ***
Go To Login Page
	Open Browser	${LoginURL}	${Browser}



